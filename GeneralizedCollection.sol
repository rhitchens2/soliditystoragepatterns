pragma solidity ^0.4.6;

/*
 * A generalized NoSql-like document storage module
 * For training purposes only. 
 * Copyright (c) 2017, Rob Hitchens. All rights reserved.
 */

contract GeneralizedCollection {
    
    // Upgrade compatibility checks matching "supportsInterface"
    
    bytes32 public supportsInterface;
    
    // Each field includes a value and a pointer to the fieldKeyIndex
    
    struct FieldStruct {
        bytes32 value;
        uint fieldKeyListPointer;
    }
    
    // Each record supports enumerating key/value pairs stored
    
    struct RecordStruct {
        mapping(uint => FieldStruct) fieldStructs;
        uint[] fieldKeyList;
        uint recordListPointer;
    }
    
    // Each collection supports enumerating the primary keys stored
    
    mapping(bytes32 => RecordStruct) private recordStructs;
    bytes32[] private recordList;
    
    function GeneralizedCollection(bytes32 name) {
        supportsInterface = sha3(name);
    }
    
    // Count records in the collection
    
    function getRecordCount() 
        public
        constant
        returns(uint recordCount)
    {
        return recordList.length;
    }
    
    // Count fields in a record
    
    function getRecordFieldKeyCount(bytes32 pk) 
        public
        constant
        returns(uint keyCount)
    {
        if(!isRecord(pk)) throw;
        return(recordStructs[pk].fieldKeyList.length);
    }
    
    // Check a primary key
    
    function isRecord(bytes32 pk)
        public
        constant
        returns(bool isIndeed)
    {
        if(recordList.length==0) return false;
        return(recordList[recordStructs[pk].recordListPointer]==pk);
    }
    
    // Check a field key in a specific record
    
    function isRecordFieldKey(bytes32 pk, uint fieldKey)
        public
        constant
        returns(bool isIndeed)
    {
        if(!isRecord(pk)) return false;
        if(getRecordFieldKeyCount(pk)==0) return false;
        return(recordStructs[pk].fieldKeyList[recordStructs[pk].fieldStructs[fieldKey].fieldKeyListPointer] == fieldKey);
    }

    // Insert a primary key. Field count defaults to 0.
    
    function insertRecord(bytes32 pk)
        private
        returns(bool success)
    {
        if(isRecord(pk)) throw;
        recordStructs[pk].recordListPointer = recordList.push(pk) -1;
        return true;
    }
    
    // Insert a field key in a specific record. Value default to 0x0
    
    function insertRecordField(bytes32 pk, uint fieldKey)
        private
        returns(bool success)
    {
        if(!isRecord(pk)) throw;
        if(isRecordFieldKey(pk,fieldKey)) throw;
        recordStructs[pk].fieldStructs[fieldKey].fieldKeyListPointer = recordStructs[pk].fieldKeyList.push(fieldKey)-1;
        return true;
    }
    
    // Set a field key value in a specific record. Insert the primary key and/or field key if needed.
    
    function upsertRecordField(bytes32 pk, uint fieldKey, bytes32 value)
        // secure this
        public
        returns(bool success)
    {
        if(!isRecord(pk)) insertRecord(pk);
        if(!isRecordFieldKey(pk, fieldKey)) insertRecordField(pk,fieldKey);
        recordStructs[pk].fieldStructs[fieldKey].value = value;
        return true;
    }
    
    // Get a field key value from a specific record
    
    function getRecordFieldValue(bytes32 pk, uint fieldKey) 
        public
        constant
        returns(bytes32 value)
    {
        if(!isRecordFieldKey(pk, fieldKey)) throw;
        return recordStructs[pk].fieldStructs[fieldKey].value;
    }
    
    // Delete a complete record. Useful for reducing list size.
    
    function deleteRecord(bytes32 pk)
        // secure this
        public
        returns(bool success)
    {
        if(!isRecord(pk)) throw;
        uint rowToDelete = recordStructs[pk].recordListPointer;
        bytes32 keyToMove   = recordList[recordList.length - 1];
        recordStructs[keyToMove].recordListPointer = rowToDelete;
        recordList[rowToDelete] = keyToMove;
        recordList.length--;
        return true;
    }
    
    // Delete a field key/value from a record. Useful for reducing list size. 
    
    function deleteRecordField(bytes32 pk, uint fieldKey)
        // secure this
        public
        returns(bool success)
    {
        if(!isRecordFieldKey(pk, fieldKey)) throw;
        uint rowToDelete = recordStructs[pk].fieldStructs[fieldKey].fieldKeyListPointer;
        uint recordFieldCount = recordStructs[pk].fieldKeyList.length;
        uint keyToMove   = recordStructs[pk].fieldKeyList[recordFieldCount-1];
        recordStructs[pk].fieldStructs[keyToMove].fieldKeyListPointer = rowToDelete;
        recordStructs[pk].fieldKeyList[rowToDelete] = keyToMove;
        recordStructs[pk].fieldKeyList.length--;
        return true;
    }  
    
}