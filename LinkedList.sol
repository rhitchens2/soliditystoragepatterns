pragma solidity ^0.4.6;

// Hints should be valid Id or "0x0" to insert into empty list. 
// Hints should be CLOSE to ordered insertion point to save gas.
// This is WIP

contract LinkedList {
    
    bytes32 public firstEntity;
    bytes32 public lastEntity;
    
    struct EntityStruct {
        uint thisRow;
        bytes32 nextEntity;
        bytes32 previousEntity;
    }
    
    mapping(bytes32 => EntityStruct) public entityStructs;
    bytes32[] public entityList;

    function isEntity(bytes32 entityId)
        public
        constant
        returns(bool isIndeed)
    {
        if(entityList.length==0) return false;
        return entityList[entityStructs[entityId].thisRow] == entityId;
    }
    
    function isFirstEntity(bytes32 entityId)
        public
        constant
        returns(bool isIndeed)
    {
        if(entityId==firstEntity) return true;
    }
    
    function isLastEntity(bytes32 entityId)
        public
        constant
        returns(bool isIndeed)
    {
        if(entityId==lastEntity) return true;
    }    
    
    function getEntityCount()
        public
        constant
        returns(uint entityCount)
    {
        return entityList.length;
    }
    
    function getPreviousEntity(bytes32 entityId)
        public
        constant
        returns(bytes32 previousId)
    {
        if(!isEntity(entityId)) throw;
        if(isFirstEntity(entityId)) throw;
        return entityStructs[entityId].previousEntity;
    }
    
    function getNextEntity(bytes32 entityId)
        public
        constant
        returns(bytes32 previousId)
    {
        if(!isEntity(entityId)) throw;
        if(isLastEntity(entityId)) throw;
        return entityStructs[entityId].nextEntity;
    }
    
    function getHigherThan(bytes32 searchId, bytes32 hintId)
        public
        constant
        returns(bytes32 higherId)
    {
        if(!isEntity(hintId)) throw;
        if(hintId > searchId) { 
            if(isFirstEntity(hintId)) return hintId;
            if(entityStructs[hintId].previousEntity > searchId) {
               return getHigherThan(searchId, entityStructs[hintId].previousEntity); 
            } else {
               return hintId;
            }
        }
        if(isLastEntity(hintId)) return 0;
        return getHigherThan(searchId,entityStructs[hintId].nextEntity);
        
    }
    
    function getLowerThan(bytes32 searchId, bytes32 hintId)
        public
        constant
        returns(bytes32 higherId)
    {
        if(!isEntity(hintId)) throw;
        if(hintId < searchId) {
            if(isLastEntity(hintId)) return hintId;
            if(entityStructs[hintId].nextEntity < searchId) {
                return getLowerThan(searchId,entityStructs[hintId].nextEntity);
            } else {
                return hintId;
            }
        }
        if(isFirstEntity(hintId)) return 0;
        return getLowerThan(searchId,entityStructs[hintId].previousEntity);
    }
    
    // Insert entityId in ordered list. 
    // Hint is a starting point for the search for the exact insertion point.
    // Hint must be an existing entityId unless the list is empty.
    // Good hints prevent gas cost increases.
    // Will search for the correct insertion point or run out of gas trying. 
    
    function insertEntity(bytes32 entityId, bytes32 hintId) 
        public
        returns(bool success)
    {
        if(entityId==0) throw;
        if(!isEntity(hintId) && getEntityCount() > 0) throw; // non-existant hint
        if(isEntity(entityId) && getEntityCount() > 0) throw; // duplicates prohibited
        
        if(entityList.length==0) {
            entityStructs[entityId].thisRow = entityList.push(entityId) - 1;
            firstEntity = entityId;
            lastEntity = entityId;
            return true;
        }
        
        bytes32 lowerThanEntity = getLowerThan(entityId, hintId);
        bytes32 higherThanEntity = getHigherThan(entityId, hintId);
        
        if(lowerThanEntity==0) {
            firstEntity = entityId;
        } else {
            entityStructs[lowerThanEntity].nextEntity = entityId;
            entityStructs[entityId].previousEntity = lowerThanEntity;            
        }
        
        if(higherThanEntity==0) {
            lastEntity = entityId;
        } else {
            entityStructs[higherThanEntity].previousEntity = entityId;
            entityStructs[entityId].nextEntity = higherThanEntity;            
        }
        
        entityStructs[entityId].thisRow = entityList.length;
        entityList.push(entityId);
        return true;
    }
    
    function deleteEntity(bytes32 entityId)
        public
        returns(bool success)
    {
        if(!isEntity(entityId)) throw;
        
        if(entityId == firstEntity) {
            entityStructs[entityStructs[entityId].nextEntity].previousEntity = 0;
            firstEntity = entityStructs[entityId].nextEntity;
        } else {
            entityStructs[entityStructs[entityId].nextEntity].previousEntity = entityStructs[entityId].previousEntity;
        }
        
        if(entityId == lastEntity) {
            entityStructs[entityStructs[entityId].previousEntity].nextEntity = 0;
            lastEntity = entityStructs[entityId].previousEntity;
        } else {
            entityStructs[entityStructs[entityId].previousEntity].nextEntity = entityStructs[entityId].nextEntity;
        }
        
        uint rowToDelete = entityStructs[entityId].thisRow;
        bytes32 keyToMove = entityList[entityList.length-1];
        
        entityList[rowToDelete] = keyToMove;
        entityStructs[keyToMove].thisRow = rowToDelete;
        entityList.length--;
        return true;
    }
}