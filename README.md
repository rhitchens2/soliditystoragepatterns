# Solidity Storage Patterns#

## Idiomatic Data Storage Patterns for Ethereum Solidity 

### SimpleStoragePatterns.sol  

* Common simple patterns for organizing contract data including random access. 
* Iteration and delete-capable indexes. 
* [more info...](http://ethereum.stackexchange.com/questions/13167/are-there-well-solved-and-simple-storage-patterns-for-solidity) 
* Consistent gas cost at any scale.

### SolidityCRUD-part2.sol 

* Create. Retrieve. Update. Delete. 
* "UserCRUD" example implementation of Mapped Structs with Delete-Capable Index pattern. 
* [docs/code explanation ... ](https://medium.com/@robhitchens/solidity-crud-part-1-824ffa69509a)
* Consistent gas cost at any scale.

### OneToMany.sol  

* Referential Integrity Enforcement using two SolidityCRUD entities in a one-to-many relationship. 
* Two-way binding and enforcement of referential integrity. 
* [docs/code explanation](https://medium.com/@robhitchens/enforcing-referential-integrity-in-ethereum-smart-contracts-a9ab1427ff42)
* Consistent gas cost at any scale. 

### LinkedList.sol

* Searchable ordered list with Insert and Delete. Iteration in ascending or descending order. 
* Consistent gas cost at any scale.
* Uses client-side "hints" to discover search starting points near correct insertion/deletion point.

### GeneralizedCollection.sol

* A NoSql generalized storage contract
* Suitable for data/logic separation in upgradable contracts
* Fields implemented as key/value records and stored by primary keys
* Enumerate primary keys and field keys (per record).
* C.R.U.D. operations at instance/document/record and record field level.