pragma solidity ^0.4.6;

contract simpleList {
    
    struct EntityStruct {
        address entityAddress;
        uint entityData;
        // more fields
    }
    
    EntityStruct[] public entityStructs;
    
    function newEntity(address entityAddress, uint entityData) public returns(uint rowNumber) {
        EntityStruct memory newEntity;
        newEntity.entityAddress = entityAddress;
        newEntity.entityData    = entityData;
        return entityStructs.push(newEntity)-1;
    }
    
    function getEntityCount() public constant returns(uint entityCount) {
        return entityStructs.length;
    }
}


contract mappingWithStruct {
    
    struct EntityStruct {
        uint entityData;
        bool isEntity;
    }
    
    mapping (address => EntityStruct) public entityStructs;
    
    function isEntity(address entityAddress) public constant returns(bool isIndeed) {
        return entityStructs[entityAddress].isEntity;
    }
    
    function newEntity(address entityAddress, uint entityData) public returns(bool success) {
        if(isEntity(entityAddress)) throw; 
        entityStructs[entityAddress].entityData = entityData;
        entityStructs[entityAddress].isEntity = true;
        return true;
    }
    
    function deleteEntity(address entityAddress) public returns(bool success) {
        if(!isEntity(entityAddress)) throw;
        entityStructs[entityAddress].isEntity = false;
        return true;
    }
    
    function updateEntity(address entityAddress, uint entityData) public returns(bool success) {
        if(!isEntity(entityAddress)) throw;
        entityStructs[entityAddress].entityData = entityData;
        return true;
    }
}

contract arrayWithUniqueIds {
    
    struct EntityStruct {
        address entityAddress;
        uint entityData;
    }
    
    EntityStruct[] public entityStructs;
    mapping(address => bool) knownEntity;
    
    function isEntity(address entityAddress) public constant returns(bool isIndeed) {
        return knownEntity[entityAddress];
    }
    
    function getEntityCount() public constant returns(uint entityCount) {
        return entityStructs.length;
    }
    
    function newEntity(address entityAddress, uint entityData) public returns(uint rowNumber) {
        if(isEntity(entityAddress)) throw;
        EntityStruct newEntity;
        newEntity.entityAddress = entityAddress;
        newEntity.entityData = entityData;
        knownEntity[entityAddress] = true;
        return entityStructs.push(newEntity) - 1;
    }
    
    function updateEntity(uint rowNumber, address entityAddress, uint entityData) public returns(bool success) {
        if(!isEntity(entityAddress)) throw;
        if(entityStructs[rowNumber].entityAddress != entityAddress) throw;
        entityStructs[rowNumber].entityData    = entityData;
        return true;
    }
}

// DOC: https://medium.com/@robhitchens/solidity-crud-part-1-824ffa69509a

contract mappedStructWithUnorderedIndexAndDelete {
    
    struct EntityStruct {
        uint entityData;
        uint listPointer;
    }
    
    mapping(address => EntityStruct) public entityStructs;
    address[] public entityList;
    
    function isEntity(address entityAddress) public constant returns(bool isIndeed) {
        if(entityList.length == 0) return false;
        return (entityList[entityStructs[entityAddress].listPointer] == entityAddress);
    }
    
    function getEntityCount() public constant returns(uint entityCount) {
        return entityList.length;
    }
    
    function newEntity(address entityAddress, uint entityData) public returns(bool success) {
        if(isEntity(entityAddress)) throw;
        entityStructs[entityAddress].entityData = entityData;
        entityStructs[entityAddress].listPointer = entityList.push(entityAddress) - 1;
        return true;
    }
    
    function updateEntity(address entityAddress, uint entityData) public returns(bool success) {
        if(!isEntity(entityAddress)) throw;
        entityStructs[entityAddress].entityData = entityData;
        return true;
    }
    
    function deleteEntity(address entityAddress) public returns(bool success) {
        if(!isEntity(entityAddress)) throw;
        uint rowToDelete = entityStructs[entityAddress].listPointer;
        address keyToMove   = entityList[entityList.length-1];
        entityList[rowToDelete] = keyToMove;
        entityStructs[keyToMove].listPointer = rowToDelete;
        entityList.length--;
        return true;
    }
    
}


